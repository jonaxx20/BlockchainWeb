CREATE TABLE members (
	memberID int(11) NOT NULL AUTO_INCREMENT,
	username varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	active varchar(255) NOT NULL,
	resetToken varchar(255) DEFAULT NULL,
	resetComplete varchar(3) DEFAULT 'No',
	admin varchar(3) DEFAULT 'No',
	PRIMARY KEY (memberID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE groups (
	groupID int(11) NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	PRIMARY KEY (groupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE belongs (
	memberID int(11) NOT NULL,
	groupID int(11) NOT NULL,
	PRIMARY KEY (memberID, groupID),
	FOREIGN KEY (memberID) REFERENCES members(memberID) ON DELETE CASCADE,
	FOREIGN KEY (groupID) REFERENCES groups(groupID) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
	

CREATE TABLE vms (
	vmID int(11) NOT NULL AUTO_INCREMENT,
	nebulaID int(5) NOT NULL,
	name varchar(255) NOT NULL,
	ip varchar(16) NOT NULL,
	ssh text(16383) NOT NULL,
	parent_ip varchar(16) NOT NULL,
	PRIMARY KEY (vmID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE hasaccess (
	vmID int(11) NOT NULL,
	groupID int(11) NOT NULL,
	accessLevel int(2) NOT NULL,
	PRIMARY KEY (vmID, groupID),
	FOREIGN KEY (groupID) REFERENCES groups(groupID) ON DELETE CASCADE,
	FOREIGN KEY (vmID) REFERENCES vms(vmID) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE requests (
	requestID int(11) NOT NULL AUTO_INCREMENT,
	text text(16383) NOT NULL,
	complete varchar(3) DEFAULT 'No',
	type varchar(255) DEFAULT 'Request',
	PRIMARY KEY (requestID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

