<?php
require('includes/config.php');

$memberID = $_SESSION['memberID'];
$machineip = $_POST['machineip'];
$target_dir = __DIR__ . "/uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$fileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Allow certain file formats
if($fileType != "pdf") {
    echo "Sorry, only PDF files are allowed.";
    $uploadOk = 0;
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $fileName = basename( $_FILES["fileToUpload"]["name"]);
        $hash = shell_exec("bash scripts/fileHash.sh {$target_file}");
        //echo "The file ".$fileName. " has been uploaded locally. ";
        if(isset($_POST['search'])){
            shell_exec("rm {$target_file}");
            $file=__DIR__."/scripts/{$fileName}hosts";
            $resultfile=__DIR__."/scripts/{$fileName}results.txt";
            shell_exec("touch {$file}");
            $stmt = $db->prepare('SELECT ip FROM vms');
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $content="{$row['ip']}\n";
                    file_put_contents($file, $content, FILE_APPEND | LOCK_EX);
                }
            }
            shell_exec("sudo ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook scripts/search.yaml -e 'hash={$hash} resultfile={$fileName}results' -i scripts/{$fileName}hosts");
            shell_exec("rm {$file}");
            $output = file_get_contents($resultfile);
            shell_exec("rm {$resultfile}");
            $split_output = preg_split('/[\s\n]+/',$output);
            $stmt = $db->prepare('SELECT name FROM vms WHERE ip = :ip');
            $stmt->execute(array(':ip' => $split_output[2]));
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $blockchainName = $row['name'];
            }
            $stmt = $db->prepare('SELECT username FROM members WHERE memberID = :memberID');
            $stmt->execute(array(':memberID' => $split_output[3]));
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $username = $row['username'];
            }
            $content="File {$fileName} has been uploaded<br>With Hash: {$hash}<br>To Blockchain: {$blockchainName}<br>By user: {$username}";
        } else{
            $output = shell_exec("bash scripts/file_send.sh {$machineip} {$hash} {$memberID} {$target_file} {$fileName}");
            if($output!="False"){
                header('Location: memberpage.php?action=uploaded');
                exit;
            } else{
                echo "Sorry, there was an error uploading your file to the blockchain.";
                echo '<p><a href="./">Back to home page</a></p>';
            }
        }
    } else {
        echo "Sorry, there was an error uploading your file.";
        echo '<p><a href="./">Back to home page</a></p>';
    }
}
?>

<p><a href='login.php'>Back to home page</a></p>
<?php 
    echo $content;
?>
<hr>
