#!/bin/sh
CUSER=$1
CPASS=$2
NAME=$3
PIP=$4
SSH1=$5
SSH2=$6
SSH3=$7
SSH="$SSH1 $SSH2 $SSH3"
CENDPOINT=https://grid5.mif.vu.lt/cloud3/RPC2
CVMREZ=$(onetemplate instantiate "Copy of ubuntu-16.04 Project" --name Clone$NAME --user $CUSER --password $CPASS --endpoint $CENDPOINT)
CVMID=$(echo $CVMREZ |cut -d ' ' -f 3) 
sleep 30
PVTIP=$(eval onevm show $CVMID --user $CUSER --password $CPASS  --endpoint $CENDPOINT | grep PRIVATE\_IP| cut -d '=' -f 2 | tr -d '"')
ssh -oStrictHostKeyChecking=no root@$PIP "echo '"$PVTIP"' > /root/HashingClient/scripts/ip/Clone'"$NAME"'.txt " 
ssh -oStrictHostKeyChecking=no root@$PVTIP "apt-get update -y; apt-get upgrade -y; echo '"$SSH"' >> /root/.ssh/authorized_keys; grep -vw 'www-data' /root/.ssh/authorized_keys > /root/.ssh/authorized_keys2; cat /root/.ssh/authorized_keys2 > /root/.ssh/authorized_keys; rm /root/.ssh/authorized_keys2; mkdir database; touch ./database/now.txt; echo '"$PIP"' > ./database/parentIp.txt ; echo '"$NAME"' > ./database/name.txt"
exit 0
