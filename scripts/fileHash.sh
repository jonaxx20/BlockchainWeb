#!/bin/bash
SHA=$(sha256sum $1 | awk '{ print $1 }')
echo $SHA | base64 -w 0
exit 0
