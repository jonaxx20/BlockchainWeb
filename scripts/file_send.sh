#!/bin/sh

CIP=$1
CHASH=$2
COWN=$3
CFILE=$4
CFILENAME=$5
echo $CIP $CHASH $COWN $CFILE $CFILENAME

scp -oStrictHostKeyChecking=no $CFILE root@$CIP:HashingClient/database/now/*
if [ $? -eq 0 ];
then
        echo "True"
else
        echo "False"
fi

ssh -oStrictHostKeyChecking=no root@$CIP "cd /root/HashingClient/database/now/*; touch hash$CFILENAME.txt; echo $CHASH > hash$CFILENAME.txt;touch owner$CFILENAME.txt; echo $COWN > owner$CFILENAME.txt; mv $CFILENAME File$CFILENAME"
rm $CFILE
