#!/bin/sh
CUSER=$1
CPASS=$2
NAME=$3
PIP=$4
SSH1=$5
SSH2=$6
SSH3=$7
SSH="$SSH1 $SSH2 $SSH3"
CENDPOINT=https://grid5.mif.vu.lt/cloud3/RPC2
CVMREZ=$(onetemplate instantiate "Copy of ubuntu-16.04 Project" --name $NAME --user $CUSER --password $CPASS --endpoint $CENDPOINT)
CVMID=$(echo $CVMREZ |cut -d ' ' -f 3) 
sleep 30
echo "$CVMID"
echo "$NAME"
PVTIP=$(eval onevm show $CVMID --user $CUSER --password $CPASS  --endpoint $CENDPOINT | grep PRIVATE\_IP| cut -d '=' -f 2 | tr -d '"')
echo "$PVTIP"
ssh -oStrictHostKeyChecking=no root@$PIP "echo '"$PVTIP"' > /root/HashingClient/scripts/ip/'"$NAME"'.txt " 
ssh -oStrictHostKeyChecking=no root@$PVTIP "apt-get update -y; apt-get upgrade -y; echo '"$SSH"' >> /root/.ssh/authorized_keys; cd .ssh; ssh-keygen -f id_rsa -t rsa -N ''; cd ..; apt-get install git -y; git clone https://gitlab.com/jonaxx20/HashingClient.git; apt install openjdk-8-jre -y; cd /root/HashingClient ; echo '"$PIP"' > ./database/parentIp.txt ; echo '"$NAME"' > ./database/name.txt; nohup sh -c ' java -jar HashClient.jar && java -jar HashServer.jar ' > /dev/null 2>&1 & " > /dev/null
SSH1=$(ssh -oStrictHostKeyChecking=no root@$PVTIP "cat ./.ssh/id_rsa.pub")
echo "$SSH1";
exit 0
