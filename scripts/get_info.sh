#!/bin/bash

info=$( ssh -oStrictHostKeyChecking=no root@$1 "bash /root/HashingClient/info_generate.sh; cat /root/HashingClient/blockchain_info.txt; rm /root/HashingClient/blockchain_info.txt" )

echo "$info"

