<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset='UTF-8'>
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title><?php if(isset($title)){ echo $title; }?></title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <link type='text/css' rel="stylesheet" href="style/style.css">
</head>
<body>