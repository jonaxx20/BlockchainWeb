<?php
require('includes/config.php');

if(!$user->is_logged_in() || $_SESSION['admin'] != 'Yes'){ 
	header('Location: login.php'); 
	exit(); 
}

if(isset($_GET['vmid'])){
	$stmt = $db->prepare('SELECT name FROM vms WHERE vmID = :vmID');
	$stmt->execute(array(':vmID' => $_GET['vmid']));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	$content = "<h2>Blockchain: {$row['name']}</h2>";
	$title = "Manage Blockchain:{$row['name']} group access";

	$stmt = $db->prepare('SELECT groups.name,hasaccess.accessLevel FROM groups INNER JOIN hasaccess ON hasaccess.groupID = groups.groupID WHERE vmID = :vmID');
	$stmt->execute(array(':vmID' => $_GET['vmid']));
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		$content .= "<p>{$row['name']} has access level {$row['accessLevel']}</p><br>";
	}

	$select="<select name='select'>";
	$stmt = $db->prepare('SELECT groupID, name FROM groups');
	$stmt->execute();
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		$select .="<option value='{$row['groupID']}'>{$row['name']}</option>";
	}
	$select.="</select>";

	if(isset($_POST['add'])){
		$stmt = $db->prepare('INSERT INTO hasaccess (vmID, groupID, accessLevel) VALUES (:vmID, :groupID, :accessLevel)');
			$stmt->execute(array(
				':vmID' => $_POST['vmID'],
				':groupID' => $_POST['select'],
				':accessLevel' => $_POST['level']
			));
		header('Location: '.$_SERVER['REQUEST_URI']);
	}
}

//include header template
require('layout/header.php'); 
?>

	<div>
		<p><a href='./'>Back to home page</a></p>
		<hr>
		<?php echo $content; ?>
		<form method="post" action="" id="addmember">
			<?php echo $select; ?>;
			<select name='level'>
				<option value='1'>1</option>;
				<option value='2'>2</option>;
			</select>
			<input type="hidden" name="vmID" value="<?php echo $_GET['vmid'] ?>">
			<input type='submit' value='Add Group' name='add'>
		</form>
	</div>
	


<?php 
//include footer template
require('layout/footer.php'); 
?>