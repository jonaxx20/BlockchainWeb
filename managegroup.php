<?php
require('includes/config.php');

if(!$user->is_logged_in() || $_SESSION['admin'] != 'Yes'){ 
	header('Location: login.php'); 
	exit(); 
}

if(isset($_GET['groupid'])){
	$stmt = $db->prepare('SELECT name FROM groups WHERE groupID = :groupID');
	$stmt->execute(array(':groupID' => $_GET['groupid']));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	$content = "<h2>Group: {$row['name']}</h2>";
	$title = "Manage Group:{$row['name']} members";

	$stmt = $db->prepare('SELECT username FROM members INNER JOIN belongs ON belongs.memberID = members.memberID INNER JOIN groups ON groups.groupID = belongs.groupID WHERE groups.name = :groupName');
	$stmt->execute(array(':groupName' => $row['name']));
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		$content .= "<p>--{$row['username']}</p>";
	}

	$select="<select name='select'>";
	$stmt = $db->prepare('SELECT memberID, username FROM members');
	$stmt->execute();
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		$select .="<option value='{$row['memberID']}'>{$row['username']}</option>";
	}
	$select.="</select>";

	if(isset($_POST['add'])){
		$stmt = $db->prepare('INSERT INTO belongs (memberID, groupID) VALUES (:memberID, :groupID)');
			$stmt->execute(array(
				':memberID' => $_POST['select'],
				':groupID' => $_POST['groupid']
			));
		header('Location: '.$_SERVER['REQUEST_URI']);
	}
}
//include header template
require('layout/header.php'); 
?>

	<div>
		<p><a href='./'>Back to home page</a></p>
		<hr>
		<?php 
			echo $content; 
		?>

		<form method="post" action="" id="addmember">
			<?php echo $select; ?>;
			<input type="hidden" name="groupid" value="<?php echo $_GET['groupid'] ?>">
			<input type='submit' value='Add Member' name='add'>
		</form>
	</div>
	


<?php 
//include footer template
require('layout/footer.php'); 
?>