<?php require('includes/config.php'); 

//if not logged in redirect to login page
if(!$user->is_logged_in()){ 
	header('Location: login.php'); 
	exit(); 
}
$vms = "<div>
			<h2>Blockchains</h2>";
$stmt = $db->prepare('SELECT vms.vmID, vms.name, vms.ip FROM vms INNER JOIN hasaccess ON hasaccess.vmID = vms.vmID INNER JOIN groups ON groups.groupID = hasaccess.groupID INNER JOIN belongs ON belongs.groupID = groups.groupID INNER JOIN members ON members.memberID = belongs.memberID WHERE members.memberID = :memberID');
$stmt->execute(array(':memberID' => $_SESSION['memberID']));
if ($stmt->rowCount() > 0) {
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$vms .= "<p style='display:inline-block'>{$row['name']}</p>
				<form action='upload.php' method='post' enctype='multipart/form-data'>
     			Select File to upload:
 	    			<input type='file' name='fileToUpload' id='fileToUpload'>
 	    			<input type='hidden' name='machineip' value='{$row['ip']}'>
 	    			<input type='submit' value='Upload File' name='submit'>
 				</form>
 				<hr>";
		}
	}
$vms .= "</div>";
// $vms = "";
// $stmt = $db->prepare('SELECT vmID,nebulaID,name,ip,parent_ip FROM vms WHERE memberID = :memberID');
// $stmt->execute(array(':memberID' => $_SESSION['memberID']));
// while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
// 	$output = shell_exec("bash scripts/get_info.sh {$row['ip']}");
// 	$block_output = preg_split('/[" "]/',$output,-1,PREG_SPLIT_NO_EMPTY);
// 	$block_output_length = count($block_output);
// 	echo $block_output_length;
// 	$block_info = "";
// 	for($x = 0; $x < $block_output_length; $x++) {
//     	$block_split = preg_split('/[\n]/',$block_output[$x],-1,PREG_SPLIT_NO_EMPTY);
//     	$block_split_length = count($block_split);
//     	$block_info .= "<b>Block {$block_split[0]}</b><br>";
//     	$block_info .= "<b>Hash:</b> {$block_split[1]}<br>";
//     	$timestamp = date('Y F jS h:i:s A',$block_split[2]);
//     	$block_info .= "<b>Timestamp:</b> {$timestamp}<br>";
//     	if($block_split_length>3){
//     		for($y = 3; $y < $block_split_length; $y++){
//     			$block_info .= "{$block_split[$y]}<br>";
//     		}
//     	}
//     	$block_info .= "<br>";
// 	}
// 	$vms .= "<div>
// 				<h2>Blockchain ID:{$row['nebulaID']} Name: {$row['name']} IP:{$row['ip']}</h2>
// 				<p>{$block_info}</p>
// 				<form action='upload.php' method='post' enctype='multipart/form-data'>
//     			Select File to upload:
// 	    			<input type='file' name='fileToUpload' id='fileToUpload'>
// 	    			<input type='hidden' name='machineip' value='{$row['ip']}'>
// 	    			<input type='submit' value='Upload File' name='submit'>
// 				</form>
// 			</div>";
// }

//define page title
$title = 'Members Page';

//include header template
require('layout/header.php'); 
?>

<div class="container">

	<div class="row">

	    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			
				<h2>Member only page - Welcome <?php echo htmlspecialchars($_SESSION['username'], ENT_QUOTES); ?></h2>
				<form action="">
    				<input type="submit" value="Refresh">
				</form>
				<p><a href='logout.php'>Logout</a></p>
				<hr>

				<div>
					
				</div>

				<?php
				if(isset($_GET['action'])){
					switch ($_GET['action']) {
						case 'created':
							echo "<h2 class='bg-success'>Your virtual machine has been created</h2>";
							break;

						case 'uploaded':
							echo "<h2 class='bg-success'>Your file has been uploaded</h2>";
							break;
					}
				}

				echo $vms;
				?>
				<form action='upload.php' method='post' enctype='multipart/form-data'>
    			Select file to search in system:
    				<input type='hidden' name='search' value='search'>
	    			<input type='file' name='fileToUpload' id='fileToUpload'>
	    			<input type='submit' value='Search File' name='submit'>
				</form>
		</div>
	</div>


</div>

<?php 
//include header template
require('layout/footer.php'); 
?>
