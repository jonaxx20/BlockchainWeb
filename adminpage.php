<?php require('includes/config.php'); 

//if not logged in redirect to login page
if(!$user->is_logged_in() || $_SESSION['admin'] != 'Yes'){ 
	header('Location: login.php'); 
	exit(); 
}

if(isset($_POST['create'])){
	$stmt = $db->prepare('INSERT INTO groups (name) VALUES (:name)');
			$stmt->execute(array(
				':name' => $_POST['groupname']
			));
}

$vms = "<div>
			<h2>Blockchains</h2>";
$groups = "<div>
			<h2>Groups</h2>
			<form role='form' method='post' action='' autocomplete='off'>
				<input type='text' name='groupname' placeholder='Group Name'>
				<input type='submit' name='create' value='Create Group'>
			</form>
			";

$vms .= ListChildBlockchains("10.0.1.1",$db,1);

function ListChildBlockchains($parentIP,$db,$level) {

	$stmt = $db->prepare('SELECT vmID, name, ip FROM vms WHERE parent_ip = :parentIP');
	$stmt->execute(array(':parentIP' => $parentIP));
	if ($stmt->rowCount() > 0) {
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			for($x=1;$x<$level;$x++){
				$vms .= "<span>-</span>";
			}
			$vms .= "<p style='display:inline-block'>Level:{$level} {$row['name']}</p>
				<form style='display:inline-block' action='testexec.php' method='post' enctype='multipart/form-data'>
	 	    			<input type='hidden' name='parentID' value='{$row['vmID']}'>
	 	    			<input type='submit' value='Create Child Blockchain' name='submit'>
				</form>
				<form style='display:inline-block' action='manageaccess.php' method='get' enctype='multipart/form-data'>
	 	    			<input type='hidden' name='vmid' value='{$row['vmID']}'>
	 	    			<input type='submit' value='Manage Group Access'>
				</form>";
			$vms .= ListChildBlockchains($row['ip'],$db,++$level);
			$level--;
			if($level==1){
				$vms .= "<hr>";
			}
		}
	}
	return $vms;
}

$stmt = $db->prepare('SELECT groupID, name FROM groups');
$stmt->execute();
while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	$groups .= "<p>{$row['name']}</p>
				<div>";
	$stmt2 = $db->prepare('SELECT username FROM members INNER JOIN belongs ON belongs.memberID = members.memberID INNER JOIN groups ON groups.groupID = belongs.groupID WHERE groups.name = :groupName');
	$stmt2->execute(array(':groupName' => $row['name']));
	while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)){
		$groups .= "<p>--{$row2['username']}</p>";
	}
	$groups .= "<form action='managegroup.php' method='get' enctype='multipart/form-data'>
	 	    		<input type='hidden' name='groupid' value='{$row['groupID']}'>
	 	    		<input type='submit' value='Manage Group Members'>
				</form>
				</div>
				<hr>";
}
$vms .= "</div>";
$groups .= "</div>";
//define page title
$title = 'Admin Page';

//include header template
require('layout/header.php'); 
?>

<div class="container">

	<div class="row">

	    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			
				<h2>Member only page - Welcome <?php echo htmlspecialchars($_SESSION['username'], ENT_QUOTES); ?></h2>
				<form action="">
    				<input type="submit" value="Refresh">
				</form>
				<p><a href='logout.php'>Logout</a></p>
				<hr>

				<div>
					<form action="testexec.php">
    					<input type="submit" value="Create Blockchain">
					</form>
				</div>

				<?php
				if(isset($_GET['action'])){
					switch ($_GET['action']) {
						case 'created':
							echo "<h2 class='bg-success'>Your virtual machine has been created</h2>";
							break;

						case 'uploaded':
							echo "<h2 class='bg-success'>Your file has been uploaded</h2>";
							break;
					}
				}

				echo $vms;
				echo $groups;

				?>

		</div>
	</div>


</div>

<?php 
//include footer template
require('layout/footer.php'); 
?>