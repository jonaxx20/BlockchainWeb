<?php
require('includes/config.php');

if(!$user->is_logged_in()){ 
	header('Location: login.php'); 
	exit(); 
}

if(isset($_POST['parentID'])){
	$stmt = $db->prepare('SELECT ip, ssh FROM vms WHERE vmID = :vmID');
	$stmt->execute(array(':vmID' => $_POST['parentID']));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$parent_IP = $row['ip'];
	$parent_ssh = $row['ssh'];
}else{
	$parent_IP = "10.0.1.1";
	$parent_ssh = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDiwQUprvocg6SHg5tD1JRAd4ZGBZgvq+FqZYjARhdi8OKC3NkADhOcieCysUKi9bkrMy7HiZab7AWO1G+t+hDt9MLUCMRps+N1rus+hD/LU9ByljjBYdnmjL/dMoL2OqB48Muhw/d8QKGbnvRFQgIl9DCGchjMMwxUr7TsoOPsqdAuZSdk2XWeSAn1lLhohvWaVs5dScUJMSMBd8dBYXgIE3+chaeB/p9vubGE0AaUu6URpMIfwEUbLCJC5f3MTQflYuEVanhvpGwpgyZ858+NZDTZueqxdYSVWGD/L+z/dVPPs/HmNqLD9cRq4YIlZF8enyygGiVAqZg8JbbAabYX root@ubuntu";
}

if(isset($_POST['create'])){

	if (!isset($_POST['nebulaname'])) $error[] = "Please fill out name";
	if (!isset($_POST['nebulapass'])) $error[] = "Please fill out password";
	if (!isset($_POST['nebulavmname'])) $error[] = "Please fill out all machine name";

	$name = $_POST['nebulaname'];
	$pass = $_POST['nebulapass'];
	$vmname = $_POST['nebulavmname'];
	$parentIP = $_POST['parentip'];
	$parentssh = $_POST['parentssh'];

	if(!isset($error)){
		try{
			shell_exec("bash scripts/CreateCloneVM.sh {$name} {$pass} {$vmname} {$parentIP} {$parentssh} ");
			$output = shell_exec("bash scripts/CreateVM.sh {$name} {$pass} {$vmname} {$parentIP} {$parentssh} ");
		} catch(PDOException $e) {
			$error[] = $e->getMessage();
		}
	}


	if(!isset($error)){
		echo $output.' | ';
		$split_output = preg_split('/[\s\n]+/',$output);
		//echo "{$split_output[0]} {$split_output[1]} {$split_output[2]} {$split_output[3]} {$split_output[4]} {$split_output[5]} {$parentIP}";
		try{
			$stmt = $db->prepare('INSERT INTO vms (nebulaID,name,ip,ssh,parent_ip) VALUES (:nebulaID, :name, :ip, :ssh, :parent_ip)');
			$stmt->execute(array(
				':nebulaID' => $split_output[0],
				':name' => $split_output[1],
				':ip' => $split_output[2],
				':ssh' => "{$split_output[3]} {$split_output[4]} {$split_output[5]}",
				':parent_ip' => $parentIP
			));
		} catch(PDOException $e) {
			$error[] = $e->getMessage();
		}
		header('Location: adminpage.php?action=created');
		exit;
	}else{
		echo "VM could not be created";
		foreach($error as $error){
			echo '<p>'.$error.'</p>';
		}
		echo '<p><a href="./">Back to home page</a></p>';
	}
}
?>

<form role="form" method="post" action="" autocomplete="off">
				<h2>Please Enter OpenNebula Login</h2>
				<p><a href='login.php'>Back to home page</a></p>
				<hr>

				<?php
				//check for any errors
				if(isset($error)){
					foreach($error as $error){
						echo '<p class="bg-danger">'.$error.'</p>';
					}
				}
				?>
				<div class="form-group">
					<input type="text" name="nebulaname" id="nebulaname" class="form-control input-lg" placeholder="User Name" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['username'], ENT_QUOTES); } ?>" tabindex="1">
				</div>

				<div class="form-group">
					<input type="password" name="nebulapass" id="nebulapass" class="form-control input-lg" placeholder="Password" tabindex="3">
				</div>

				<div class="form-group">
					<input type="text" name="nebulavmname" id="nebulavmname" class="form-control input-lg" placeholder="Machine Name" tabindex="3">
					<input type="hidden" name="parentip" value="<?php echo $parent_IP ?>">
					<input type="hidden" name="parentssh" value="<?php echo $parent_ssh ?>">
				</div>
				<hr>
				<div class="row">
					<div class="col-xs-6 col-md-6"><input type="submit" name="create" value="Submit" class="btn btn-primary btn-block btn-lg" tabindex="5"></div>
				</div>
</form>
